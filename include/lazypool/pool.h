#pragma once
#include "stack.h"
#include "area.h"

namespace lazypool {

class Pool
{
public:
  Pool() : chunck_size(0) {};
  ~Pool() { if(size>0) free_areas();};
  
  void init(size_t _chunck_size,size_t _area_size=100, unsigned int _grow_factor_times_ten=14)  throw (std::bad_alloc)
  {
    assert(chunck_size==0); // assert not already initialized.
    chunck_size=_chunck_size;
    area_size=_area_size;
    size=0;
    grow_factor_times_ten=_grow_factor_times_ten;
    grow();
  };

  void free_chunk(void *p) {free_solo_stack.push(p);};
  
  void *get_chunck()  throw (std::bad_alloc) {
    if(free_solo_stack.is_empty())
    {
      if(free_area_stack.top().is_empty())
        grow();
      return free_area_stack.top().get_chunck(chunck_size);
    }
    else
      return free_solo_stack.get_pop();
  };
  
  // Grow by area_size chuncks.
  void grow()  throw (std::bad_alloc) {
    // We need a new area and to grow the solo stack of this number of chunck.
    free_area_stack.grow(1);
    free_solo_stack.grow(area_size);
    size_t area_size_bytes=chunck_size*area_size;
    size+=area_size;
    void *start_area=malloc(area_size_bytes);
    if(start_area==nullptr) throw  std::bad_alloc();
    area_size=area_size*grow_factor_times_ten/10; // Compute the number of chunks to allocate next time
    free_area_stack.push(Area(start_area, reinterpret_cast<char *>(start_area)+area_size_bytes));
  };
  
  // All chunks are unused ?
  bool is_empty() { 
    return free_area_stack.top().get_size(chunck_size) + free_solo_stack.get_size() == size; 
  };
  
private:
  // Free all ereas. Must be called only for a clean destruction.
  void free_areas()
  {
    assert(is_empty());
    // For all area free the memory.
    size_t max=free_area_stack.get_size();
    for(size_t n=0; n<max; n++)
      free_area_stack.get_elem(n).free_area();
    // this is now unusable.
  }
  
protected:
  // Stack of free areas, only the top can be non-empty, we keep the others to have pointers on chuck areas.
  Stack<Area> free_area_stack; 
  // Stack of pointers on free chunks.
  Stack<void *> free_solo_stack; 
  size_t chunck_size;
  size_t area_size; // Number of chuncks in the next area size to allocate.
  size_t size; // The actual capacity (free or not) in chuncks.
  unsigned int grow_factor_times_ten;
};

} 
