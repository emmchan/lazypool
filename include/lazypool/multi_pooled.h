#pragma once
// Like "pooled.h" but handle multiple size dynamicaly. So the class can have children with different sizes.
// Every chunck start by the address of its pool at the start, so we can recognize the pool to use on delete. We could probably have used tricky things like reviving dead objects to use less resources, but it would be immoral. Multiple classes not related by some inheritance can share the use of the MultiPooled class, then we don't have to use the id trick from Pooled.
#include "vector.h"
#include "pool.h"

namespace lazypool {

class MultiPooled
{
public:
  void* operator new(size_t n) throw(std::bad_alloc) { 
    Pool *p=vector.get_pool(n+sizeof(Pool *));
    Pool **q=reinterpret_cast<Pool **>(p->get_chunck());
    *q=p; // Store the pool address at the start of the chunck
    return reinterpret_cast<void *>(q+1);
  };
  void operator delete(void * q) throw() {
    Pool ** qm1=reinterpret_cast<Pool **>(q)-1; // q is the address of the object, qm1 is the address of the chunck
    Pool *p = *qm1; // Get the address of the pool from the chunck (see new).
    p->free_chunk(reinterpret_cast<void *>(qm1));
  };
  
  // Placement : do nothing but must be overloaded too.
  void* operator new (std::size_t /*size*/, void* ptr) throw() {return ptr;};
  void operator delete (void* /*ptr*/, void* /*voidptr2*/) throw() {};
  
protected:
  static Vector vector; // When the vector is destructed, he destroy all pools and its array.
};

}
