#pragma once
#include <stdlib.h>
#include <new>
#include <assert.h>
namespace lazypool {

template<class T> class Stack
{
public:
  Stack() : stack(nullptr),elements(0),size(0) {};
  ~Stack() { free(reinterpret_cast<void *>(stack)); size=0; }
  
  bool is_empty() const { return elements == 0; };
  bool is_full() const { return elements == size; };
  T get_pop() { assert(size>0); elements--; return stack[elements]; };
  void pop() { assert(size>0); elements--; };
  void push(T x) { 
    assert(size>0); // Test if the Pool is destroyed before objects in it.
    stack[elements]=x; 
    elements++;
  };
  T & top() { return stack[elements-1]; }
  T & get_elem(size_t n) { return stack[n]; }
  void grow(size_t n) {
    stack=reinterpret_cast<T *>(realloc(stack,(size+n)*sizeof(T)));
    if(stack==nullptr) throw std::bad_alloc();
    size+=n;
  };
  size_t get_size() { return elements;};
protected:
  T * stack;
  size_t elements; // number of elements
  size_t size; // The maximum number of elements
};

}
