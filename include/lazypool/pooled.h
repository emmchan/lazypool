#pragma once
// A parent of a class to manage with a pooling memory allocation. Need to manage only objects created and destroyed durint the main function execution. Because this class have global data destroyed anytime in the global variables destruction time.

#include <stdlib.h>

#include "pool.h"

namespace lazypool {


typedef void * (*InitOrAllocT)(size_t n);

template<int N> class Pooled
{
public:
  void* operator new(size_t n) { return (*init_or_alloc)(n);};
  void operator delete(void*p) { pool.free_chunk(p);};
private:
  static void* init(size_t n) { 
    pool.init(n); 
    init_or_alloc=&Pooled<N>::alloc; 
    return pool.get_chunck();
  };
  static void* alloc(size_t /*n*/) { return pool.get_chunck(); };
    
  // Placement : do nothing but must be overloaded too.
  void* operator new (std::size_t /*size*/, void* ptr) throw() {return ptr;};
  void operator delete (void* /*ptr*/, void* /*voidptr2*/) throw() {};

  static Pool pool;
  static InitOrAllocT init_or_alloc;
};


#define POOLED_GLOBS(NUM) \
template <> lazypool::Pool lazypool::Pooled<NUM>::pool=lazypool::Pool(); \
template <> lazypool::InitOrAllocT lazypool::Pooled<NUM>::init_or_alloc=&lazypool::Pooled<NUM>::init;

}
