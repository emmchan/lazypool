#pragma once
#include <assert.h>
#include <stdlib.h>

namespace lazypool {

class Area
{
public:
  Area(void *_start, void *_next_end) : start(_start),next_end(_next_end),initial_start(_start) {};
  // Area is copied when passed by argument, so we can't free the area (initial_start) on destruction.
  
  bool is_empty() { return start==next_end;};
  
  // Return the address of a free chunck.
  void *get_chunck(size_t chunck_size) { 
    assert(!is_empty()); 
    void * res = start; start =reinterpret_cast<char *>(start) + chunck_size; 
    return res; 
  };
  
  // Number of free chucks
  size_t get_size(size_t chunck_size) { return ((reinterpret_cast<char *>(next_end)-reinterpret_cast<char *>(start))/chunck_size); };
  
  void free_area() { free(initial_start); };
  
protected:
  void * start; // The actual starting area
  void * next_end; // pointer to the first element after the end of the area
  void * initial_start; // The start value at initialization.
};

}
