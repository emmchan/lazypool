#pragma once
// A vector with arbitrary start & end index, resizable both side, fill with zero new places.
#include <stdlib.h>
#include <string.h> // For memset
#include <assert.h>

#include "pool.h"

namespace lazypool {

class Vector
{
public:
  Vector(size_t _area_size=100, unsigned int _grow_factor_times_ten=14) :
    chunck_start_size(0),chunck_end_size(0),array(nullptr),grow_factor_times_ten(_grow_factor_times_ten),area_size(_area_size) {};
    
  ~Vector() {
    // Delete pools
    size_t n;
    Pool *p;
    for(n=chunck_start_size; n<chunck_end_size; n++) {
      p=array[n];
      if(p!=nullptr)
        delete p;
    }
    // Delete the array.
    free(reinterpret_cast<void *>(array+chunck_start_size));
  }
  
  // Return the pool from the size of the chunk. Create a pool if it doesn't exists. Initialize the Vector if it's not done.
  Pool * get_pool(size_t chunck_size) {
    assert(chunck_size>0);
    // Do we need to init the vector ?
    if(chunck_size >= chunck_end_size) // If the requested chunck size is too high
    {
      Pool **p;
      if(chunck_end_size==0) // If first initialization.
      {
        p=reinterpret_cast<Pool **>(malloc(sizeof(Pool *)));
        if(p==nullptr) throw  std::bad_alloc();
        chunck_start_size=chunck_size;
        chunck_end_size=chunck_start_size+1;
        array=p - chunck_start_size;
        array[chunck_size]=nullptr;
      }
      else
      {
        size_t new_chunck_end_size=chunck_size+1;
        p=reinterpret_cast<Pool **>(realloc(reinterpret_cast<void *>(array+chunck_start_size),(new_chunck_end_size-chunck_start_size)*sizeof(Pool *)));
        if(p==nullptr) throw  std::bad_alloc();
        array=p - chunck_start_size;
        memset(reinterpret_cast<void *>(&(array[chunck_end_size])), 0, (new_chunck_end_size-chunck_end_size)*sizeof(Pool *));
        chunck_end_size=new_chunck_end_size;
      }
    }
    else
    {
      if(chunck_size<chunck_start_size) // If the requested chunck size is too low
      {
        Pool **p;
        p=reinterpret_cast<Pool **>(realloc(reinterpret_cast<void *>(array+chunck_start_size),(chunck_end_size-chunck_size)*sizeof(Pool *)));
        if(p==nullptr) throw  std::bad_alloc();
        array=p - chunck_size;
        memset(reinterpret_cast<void *>(p), 0, (chunck_start_size-chunck_size)*sizeof(Pool *));
        chunck_start_size=chunck_size;
      }
    }
    // Now the array is correct
    Pool *p=array[chunck_size];
    if(p==0) // If there is no pool for this size, then we create it.
    {
      p=new Pool();
      p->init(chunck_size,area_size,grow_factor_times_ten);
      array[chunck_size]=p;
    }
    // Now p is a pool
    return p;
  };
protected:
  size_t chunck_start_size; // Size of the chunck for the first pointer in the vector.
  size_t chunck_end_size; // Size of the chunck for the first pointer out of the vector (by the right).
  Pool **array;
  unsigned int grow_factor_times_ten; // For new pool initialization, grow factor of the area (see pool.h).
  size_t area_size; // For new pool initialization, size (in chunks) of the first area.
};
}
