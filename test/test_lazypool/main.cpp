#include <iostream>
#include <stdio.h>

#include "a.h"
#include "pool_choice.h"

#ifdef USE_MULTI_POOLED
#include "bcde_multipooled.h"
lazypool::Vector lazypool::MultiPooled::vector;
#endif

#ifdef USE_POOLED
#include "bcde_pooled.h"
POOLED_GLOBS(1)
POOLED_GLOBS(2)
POOLED_GLOBS(3)
POOLED_GLOBS(4)
#endif

#ifdef USE_NO_POOL
#include "bcde.h"
#endif



size_t A::max_allocs=10000;
size_t A::max_actions=1000000;
int A::max_actions_per_child=10;
size_t A::max_buff_size=200;
bool A::alloc_buff=true;


void run_test()
{
  B b;

  while(A::actions < A::max_actions) {
    b.random_action();
  };
  std::cout << "--- Before destruction : \nActions done : " << A::actions << "\n" 
    << "Number of objects or buffers at this time: " << A::allocs << "\n"
    << "Number of objects construction : " << A::obj_created << "\n"
    << "Number of buffers created : " << A::buff_created << "\n";
}

int main(int /*argc*/, char **/*argv*/) {
  run_test();
  std::cout << "\n--- Number of objects or buffers after destruction of the root object: " << A::allocs <<"\n";

  return 0;
}
