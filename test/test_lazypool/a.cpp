#include <stdlib.h>
#include <assert.h>
#include <string>

#include "a.h"
#include "pool_choice.h"

#ifdef USE_MULTI_POOLED
#include "bcde_multipooled.h"
#endif
#ifdef USE_POOLED
#include "bcde_pooled.h"
#endif
#ifdef USE_NO_POOL
#include "bcde.h"
#endif

size_t A::allocs=0;
size_t A::actions=0;
size_t A::obj_created=0;
size_t A::buff_created=0;


A::A() { 
  allocs++;
  obj_created++;
  init_array(a,10); 
  buff=nullptr;
  child[0]=child[1]=nullptr;
}

A::~A() throw() 
{ 
  allocs--;
  check_array(a,10);  
  del_child(0);
  del_child(1);
  del_buff();
}

void A::random_action()
{
  if(actions>=max_actions)
    return;
  actions++;
  int r=rand() % 21;
  
  if(r== 0)              add_buff();
  else if(r== 1)         del_buff();
  else if(r>1 && r<=10)  add_child(r%2);
  else if(r>10 && r<=20) del_child(r%2);
}

A * A::factory()
{
  switch(rand() % 4)
  {
    case 0:
      return new B();
      break;

    case 1:
      return new C();
      break;

    case 2:
      return new D();
      break;

    case 3:
      return new E();
      break;

    default:
      assert(false);
      return new B();
      break;
  }
}

void A::add_buff()
{
  if(allocs>=max_allocs || buff!=nullptr || !alloc_buff)
    return;
  buff=reinterpret_cast<char *>(malloc(rand()%(max_buff_size-10)+10));
  if(buff==nullptr)
    throw std::bad_alloc();
  allocs++;
  buff_created++;
}

void A::del_buff()
{
  if(buff==nullptr || !alloc_buff)
    return;
  free(buff);
  buff=nullptr;
  allocs--;
}

void A::add_child(unsigned int n)
{
  if(allocs>=max_allocs || child[n]!=nullptr)
    return;
  A * c=factory();
  child[n]=c;
  for(unsigned int m=rand()%max_actions_per_child; m>0; m--)
    c->random_action();
}


void A::del_child(unsigned int n)
{
  if(child[n]==nullptr)
    return;
  delete (child[n]);
  child[n]=nullptr;
}


void A::init_array(short unsigned int *p, unsigned int size) 
{ 
  for(short unsigned int n=0; n<size; n++)
    p[n]=n;
}

void A::check_array(short unsigned int *p, unsigned int size) throw() 
{ 
  for(short unsigned int n=0; n<size; n++)
  {
    if(p[n]!=n)
      throw std::runtime_error(std::string("Memory error while checking an array.!"));
  }
}
