#pragma once
// Base class
#include <string>
#include <stdio.h>
#include <stdexcept>

class A
{
public:
  A();
  virtual ~A() throw();
  
  // Run one of the actions functions (add/del)/(buff/child)
  void  random_action();
  
  // Generate one object of one of the A children classes.  
  A * factory();
  
  // Add a buffer, using malloc.  
  void add_buff();
  
  // Del the buffer if he is allocated.
  void del_buff();
  
  // Add one of the 2 possible child if it doesn't exist.
  void add_child(unsigned int n);
  
  // Del the child n if he exists.
  void del_child(unsigned int n);
  
  // Initialize the internal array. Used to check integrity.  
  void init_array(short unsigned int *p, unsigned int size);
  
  // Check if the internal array has the right values. 
  void check_array(short unsigned int *p, unsigned int size) throw();

  // Return the name, used to have pure virtual class.
  virtual const char * my_name()=0; 
  
protected:
  A * child[2];
  short unsigned int a[10]; // Used to simulate some data and check if it is changed.
  char *buff; // used to allocate some memory
public:
  static size_t allocs; // number of memory allocation (buf or child)
  static size_t max_allocs; // maximum allocs
  static size_t actions; // number of actions done
  static size_t max_actions; // maximum number of actions
  static int max_actions_per_child; // max number of action to perform per child.
  static size_t max_buff_size; // max memory to allocate for the buffer.
  static size_t obj_created; // number of created objects
  static size_t buff_created; // number of buffers created
  static bool alloc_buff; // Do we allocate "buff" in random actions ?
};









