cmake_minimum_required(VERSION 2.6)
project(test_lazypool)

add_executable(test_lazypool main.cpp a.cpp)

install(TARGETS test_lazypool RUNTIME DESTINATION bin)

include_directories(/home/emmanuel/perso/iggi/lazi/utils/lazypool/include)
