#include "a.h"

class B : public A
{
public:
  const char * my_name() {return "B";}
protected:
  short unsigned int b[3];
};

class C : public B
{
public:
  const char * my_name() {return "C";}
protected:
  short unsigned int c[13];
};

class D : public C
{
public:
  const char * my_name() {return "D";}
protected:
  short unsigned int d[9];
};

class E : public B
{
public:
  const char * my_name() {return "E";}
protected:
  short unsigned int e[10];
};


