#include "a.h"
#include <lazypool/pooled.h>

class B : public A, public lazypool::Pooled<1>
{
public:
  const char * my_name() {return "B";}
protected:
  short unsigned int b[3];
};

class C : public A, public lazypool::Pooled<2>
{
public:
  const char * my_name() {return "C";}
protected:
  short unsigned int c[13];
};

class D : public A, public lazypool::Pooled<3>
{
public:
  const char * my_name() {return "D";}
protected:
  short unsigned int d[9];
};

class E : public A, public lazypool::Pooled<4>
{
public:
  const char * my_name() {return "E";}
protected:
  short unsigned int e[10];
};



